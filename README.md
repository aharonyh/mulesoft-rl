## The Customers API

The Customers API contains the standard CRUD methods.
It was written using the cloud tools provided by Mulesoft.
It reflects a very standard behaviour of a RESTful API, with the following:

**Types**
 - Customer: {id, firstName, lastName, addresses []}
 - Address: {line1, line2, line3, city, district, state, country, zip}

**Endpoints**
All methods return 500 for server error
On error, the message body contains a message property, plus an (optional) array of details.
 - /customers
	 - GET (a list of customers)
		 - 200 on success
	 - POST (add a customer)
		 - 201 on success, 400 for invalid customer resource, 409 if the provided customer resource already exists
 - /customers/{customerId}
	 - GET (a customer resource identified by the customerId path variable)
		 - 200 on success, 404 for missing customer resource
	 - PUT
		 - 200 on success, 400 for invalid customer resource, 404 for missing customer resource
	 - DELETE
		 - 200 on success, 404 for missing customer resource

**General Considerations**
 - KISS - I prefer to start with a simple API implementation, avoiding HTTP-specific responses (like NO-CONTENT, etc.) as much as possible, unless the target client is well known. 
 - Incremental change - I prefer to enhance the API in small steps, avoiding over-engineering.
 - HATEOAS - I prefer to add the links later, after consulting with the client developers.
 - PATCH - It is not clear if it worth to include a PATCH method for the customer resource, especially in regards to the address.

